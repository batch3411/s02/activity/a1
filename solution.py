# S02 Activity:

# 1. Get a year from the user and determine if it is a leap year or not.

# Stretch goal: Implement error checking for the leap year input
    # 1. Strings are not allowed for inputs
    # 2. No zero or negative values

year = input("Please input a year: ");

if not year.isnumeric():
    print("String value is not allowed.")
elif int(year) <= 0:
    print("Zero or negative value is not allowed.")
elif int(year) % 4 == 0:
    print(f"{year} is a leap year")
else:
    print(f"{year} is not a leap year")

# 2.  Get two numbers (row and col) from the user and create by row x col grid of asterisks given the two numbers.

row = int(input("Enter number of rows: "))
col = int(input("Enter number of columns: "))

for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()
